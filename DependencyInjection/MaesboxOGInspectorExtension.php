<?php

namespace Maesbox\OGInspectorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;


/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class MaesboxOGInspectorExtension extends Extension // implements PrependExtensionInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
//        $this->registerDoctrineMapping($config);
        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
    }
//    
//     /**
//     * Allow an extension to prepend the extension configurations.
//     *
//     * @param ContainerBuilder $container
//     */
//    public function prepend(ContainerBuilder $container)
//    {
//        // get all Bundles
//        $bundles = $container->getParameter('kernel.bundles');
//        if (isset($bundles['DoctrineBundle'])) {
//            // Get configuration of our own bundle
//            $configs = $container->getExtensionConfig($this->getAlias());
//            $config = $this->processConfiguration(new Configuration(), $configs);
//
//            // Prepare for insertion
//            $forInsertion = array(
//                'orm' => array(
//                    'resolve_target_entities' => array(
//                        'Maesbox\OGInspectorBundle\Model\OGIUserInterface' => $config['class']['user']
//                    )
//                )
//            );
//            
//            foreach ($container->getExtensions() as $name => $extension) {
//                switch ($name) {
//                    case 'doctrine':
//                        $container->prependExtensionConfig($name, $forInsertion);
//                        break;
//                }
//            }
//        }
//    }
//    
//    /**
//     * @param array $config
//     */
//    public function registerDoctrineMapping(array $config)
//    {
//        foreach ($config['class'] as $type => $class) {
//            if (!class_exists($class)) {
//                return;
//            }
//        }
//
//        $collector = DoctrineCollector::getInstance();
//
//        $collector->addAssociation($config['class']['user'], 'mapOneToMany', array(
//            'fieldName'     => 'og_players',
//            'targetEntity'  => 'Maesbox\OGInspectorBundle\Entity\OGPlayer',
//            'cascade'       => array(
//                'persist',
//            ),
//            'mappedBy'      => 'user',
//            'orphanRemoval' => true,
//        ));
//        /*
//        $collector->addAssociation($config['class']['user'], 'mapOneToMany', array(
//            'fieldName'       => 'groups',
//            'targetEntity'    => $config['class']['group'],
//            'cascade'         => array( ),
//            'joinTable'       => array(
//                'name' => $config['table']['user_group'],
//                'joinColumns' => array(
//                    array(
//                        'name' => 'user_id',
//                        'referencedColumnName' => 'id',
//                        'onDelete' => 'CASCADE'
//                    ),
//                ),
//                'inverseJoinColumns' => array( array(
//                    'name' => 'group_id',
//                    'referencedColumnName' => 'id',
//                    'onDelete' => 'CASCADE'
//                )),
//            )
//        ));*/
//    }
}
