<?php

namespace Maesbox\OGInspectorBundle\Menu;

use Knp\Menu\FactoryInterface;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\Container;

class MenuBuilder
{
    protected $factory;
    protected $container;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, Container $container)
    {
        $this->factory = $factory;
        $this->container = $container;
    }
    
    /**
     * @param RequestStack $requestStack
     * @return NavbarMenu
     */
    public function mainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        $menu->setCurrent($this->container->get('request')->getRequestUri());
        
        $menu = $this->addPlayerMenu($requestStack, $menu);
        $menu['player']->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');
        
        $menu = $this->addAllianceMenu($requestStack, $menu);
        $menu['alliance']->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');
        
        $menu = $this->addRankingMenu($requestStack, $menu);
        $menu['ranking']->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');
        
        $menu = $this->addToolsMenu($requestStack, $menu);
        $menu['tools']->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');
        //$menu->addChild("outils", array('route' => 'maesbox_oginspector_tools'))->setLabelAttributes(array('class' =>'fa fa-wrench'));
        
        //
        
        $menu = $this->addAdminMenu($requestStack, $menu);
        $menu['administration']->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown')
                ->setChildrenAttribute('class', 'dropdown-menu');
        return $menu;
    }
    
    /**
     * @param RequestStack $requestStack
     * @return SidebarMenu
     */
    public function toolsSidebar(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav sidebar-menu nav-pills nav-stacked');
        
        $menu->setCurrent($requestStack->getCurrentRequest()->getRequestUri());
        
        $menu = $this->addToolsMenu($requestStack, $menu);
        
        $menu['tools']->setChildrenAttribute('class', 'nav nav-pills nav-stacked nav-sub');
        
        
        return $menu;
    }
    
    /**
     * @param RequestStack $requestStack
     * @return SidebarMenu
     */
    public function playerSidebar(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav sidebar-menu nav-pills nav-stacked');
        
        $menu->setCurrent($requestStack->getCurrentRequest()->getRequestUri());
        
        $menu = $this->addPlayerMenu($requestStack, $menu);
        
        $menu['player']->setChildrenAttribute('class', 'nav nav-pills nav-stacked nav-sub');
        
        
        return $menu;
    }
    
    
    public function addRankingMenu(RequestStack $requestStack, $menu)
    {
        //array('route' => 'maesbox_oginspector_tools')
        $menu->addChild("ranking", array("uri" => "#ranking"))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu['ranking']->addChild("player", array('route' => 'maesbox_oginspector_tools_ogiscan'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
            $menu['ranking']->setChildrenAttribute('class', 'nav nav-pills nav-stacked nav-sub');
        $menu['ranking']['player']->addChild("general", array('route' => 'maesbox_oginspector_tools_ogiscan'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu['ranking']->addChild("alliance", array('route' => 'maesbox_oginspector_tools_xtense'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        return $menu;
    }
    
    /**
     * @param RequestStack $requestStack
     * @param RootMenu $menu
     * @return MenuComponent
     */
    public function addToolsMenu(RequestStack $requestStack, $menu)
    {
        //array('route' => 'maesbox_oginspector_tools')
        $menu->addChild("tools", array("route" => "maesbox_oginspector_tools"))
            ->setLabelAttributes(array('class' =>'fa fa-wrench'));
        
        $menu['tools']->addChild("ogiscan", array('route' => 'maesbox_oginspector_tools_ogiscan'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu['tools']->addChild("xtense", array('route' => 'maesbox_oginspector_tools_xtense'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        return $menu;
    }
    
    /**
     * @param RequestStack $requestStack
     * @param RootMenu $menu
     * @return MenuComponent
     */
    public function addPlayerMenu(RequestStack $requestStack, $menu)
    {
        
        $menu->addChild("player", array('uri' => '#player'))
            ->setLabelAttributes(array('class' =>'fa fa-wrench'));
        
        $menu['player']->addChild("profile", array('route' => 'maesbox_oginspector_profile'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu['player']->addChild("empire", array('route' => 'maesbox_oginspector_profile_empire'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        /*$menu['profile']->addChild("xtense", array('route' => 'maesbox_oginspector_tools_xtense'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));*/
        
        return $menu;
    }
    
    /**
     * @param RequestStack $requestStack
     * @param RootMenu $menu
     * @return MenuComponent
     */
    public function addAllianceMenu(RequestStack $requestStack, $menu)
    {
        
        $menu->addChild("alliance", array('uri' => '#alliance'))
            ->setLabelAttributes(array('class' =>'fa fa-wrench'));
        
        $menu['alliance']->addChild("informations", array('route' => 'maesbox_oginspector_profile'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        /*$menu['player']->addChild("empire", array('route' => 'maesbox_oginspector_empire'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu['profile']->addChild("xtense", array('route' => 'maesbox_oginspector_tools_xtense'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));*/
        
        return $menu;
    }
    
    public function addAdminMenu(RequestStack $requestStack, $menu)
    {
        $menu->addChild("administration", array('uri' => '#admin'))
            ->setLabelAttributes(array('class' =>'fa fa-wrench'));
        
        $menu['administration']->addChild("univers", array('route' => 'maesbox_oginspector_admin_univers'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        return $menu;
    }
    
    /**
    public function mainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $request = $this->container->get('request');
        $menu->setCurrent($this->container->get('request')->getRequestUri());
        
        $session = $this->container->get('session');
//        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->container->get('doctrine.orm.entity_manager');
        
        
        
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        $route = $this->container->get('request')->get('_route');
         
        //$menu->addChild('OGI 3D', array('route' => 'maesbox_oginspector_threed_index'));
        
//        if($this->container->get('security.context')->isGranted('ROLE_OGI_ADMIN'))
//        {
//            $menu->addChild('Administration', array('route' => '#'/*'maesbox_oginspector_admin'));
//        }
        
        /*if($this->container->get('security.context')->isGranted('ROLE_USER'))
        {
            $univers = $em->getRepository('MaesboxOGInspectorBundle:Univers')->findAll();
            $nb_univers = count($univers);
            
            if($nb_univers == 1)
            {
                $selected_univers = $univers[0];
                
                $menu_name = $selected_univers->getName()." - ".$selected_univers->getLanguage();
                $selected_player = $em->getRepository('MaesboxOGInspectorBundle:OGPlayer')->findOneBy(array("univers" => $selected_univers,"user" => $user));
            
                $menu->addChild($menu_name, array('route' => 'maesbox_oginspector_univers_show', 'routeParameters' => array('id' => $selected_univers->getId())))->setLabelAttributes(array('class' =>'ion ion-planet'));
            }
            elseif ($nb_univers > 1) 
            {
                if($session->get('univers'))
                {
                    $selected_univers = $em->getRepository('MaesboxOGInspectorBundle:Univers')->find($session->get('univers'));
                    $menu_name = $selected_univers->getName()." - ".$selected_univers->getLanguage();
                    $selected_player = $em->getRepository('MaesboxOGInspectorBundle:OGPlayer')->findOneBy(array("univers" => $selected_univers,"user" => $user));
                }
                else
                {
                    $menu_name = 'Univers';
                }
                
                $menu->addChild($menu_name, array('uri' => '#Univers'))
                    ->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown');
                $menu[$menu_name]->setChildrenAttribute('class', 'dropdown-menu');
            }
            if ($nb_univers > 1)
            {
                if($session->get('univers'))
                {
                    $menu[$menu_name]->addChild($selected_univers->getName()." - ".$selected_univers->getLanguage(), array('route' => 'maesbox_oginspector_univers_show', 'routeParameters' => array('id' => $selected_univers->getId())));
                    
                    $menu[$menu_name]->addChild("Changer d'univers",array())->setAttribute('class', 'dropdown-header');
                    
                    foreach ($univers as $uni)
                    {
                        if($uni->getId() != $selected_univers->getId())
                        {
                            $menu[$menu_name]->addChild($uni->getName()." - ".$uni->getLanguage(), array('route' => 'maesbox_oginspector_univers_select', 'routeParameters' => array('id' => $uni->getId())));
                        }
                    }
                }
                else
                {
                    $menu[$menu_name]->addChild("Sélectionner un univers",array())->setAttribute('class', 'dropdown-header');

                    foreach ($univers as $uni)
                    {
                        $menu[$menu_name]->addChild($uni->getName()." - ".$uni->getLanguage(), array('route' => 'maesbox_oginspector_univers_select', 'routeParameters' => array('id' => $uni->getId())));
                    }
                }
            }
            
        }*/
        
        /*if($container->get('security.context')->isGranted('ROLE_USER'))
        {
            $menu->addChild($container->get('security.context')->getToken()->getUser()->getUsername(), array('uri' => '#'.$container->get('security.context')->getToken()->getUser()->getUsername()))
                    ->setAttribute('class', 'dropdown')
                    ->setLinkAttribute('class', 'dropdown-toggle')
                    ->setLinkAttribute('data-toggle', 'dropdown');
            
            $menu[$container->get('security.context')->getToken()->getUser()->getUsername()]->setChildrenAttribute('class', 'dropdown-menu dropdown-menu-right');
            
            $menu[$container->get('security.context')->getToken()->getUser()->getUsername()]->addChild('Mon Compte', array('route' => 'sonata_user_profile_show'));
            
            $menu[$container->get('security.context')->getToken()->getUser()->getUsername()]->addChild('Se déconnecter', array('route' => 'sonata_user_security_logout'));
            
        }
        else 
        {*/
            
//            $menu->addChild('Se connecter', array('uri' => '#' /*'sonata_user_security_login'*/));
//            $menu->addChild("S'enregistrer", array('uri' => '#' /*'sonata_user_registration_register'*/));
//        //}
//
//        $menu->addChild("Outils", array('uri' => '#Outils'))
//                        ->setAttribute('class', 'dropdown')
//                        ->setLinkAttribute('class', 'dropdown-toggle')
//                        ->setLabelAttributes(array('class' =>'fa fa-wrench'))
//                        ->setLinkAttribute('data-toggle', 'dropdown');
//        $menu["Outils"]->setChildrenAttribute('class', 'dropdown-menu dropdown-menu-right');
//        
//        $menu["Outils"]->addChild('OGI Scan', array('uri' => '#' /*'maesbox_oginspector_ogiscan_index'*/));
//        $menu["Outils"]->addChild('Xtense', array('uri' => '#' /*'maesbox_oginspector_xtense_index'*/));
//        
//        return $menu;
//    }
    
    public function sideMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav sidebar-menu nav-stacked');
        
        $menu->addChild("Classement Joueurs", array('uri' => '#'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        $menu->addChild("Classement Alliances", array('uri' => '#'))
            ->setLabelAttributes(array('class' =>'fa fa-list'));
        
        return $menu;
    }
    
    public function rechercheMenu($container)
    {
        $session = $container->get('session');
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        if($session->get('univers'))
        {
            $menu->addChild("Recherche", array('uri' => '#Recherche'))
                            ->setAttribute('class', 'dropdown')
                            ->setLabelAttributes(array('class' =>'fa fa-search'))
                            ->setLinkAttribute('class', 'dropdown-toggle')
                            ->setLinkAttribute('data-toggle', 'dropdown');
            $menu["Recherche"]->setChildrenAttribute('class', 'dropdown-menu dropdown-menu-right');

            $menu["Recherche"]->addChild('Joueurs', array("uri"=>'#') );//array('route' => 'maesbox_oginspector_ogiscan_index'));
            $menu["Recherche"]->addChild('Alliances', array("uri"=>'#'));//array('route' => 'maesbox_oginspector_xtense_index'));
            $menu["Recherche"]->addChild('Planètes colonisables', array("uri"=>'#'));
        }
        
        return $menu;
    }
    
    public function classementMenu($container)
    {
        $session = $container->get('session');
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        if($session->get('univers'))
        {
            $menu->addChild("Classement", array('uri' => '#Classement'))
                            ->setAttribute('class', 'dropdown')
                            ->setLabelAttributes(array('class' =>'fa fa-list'))
                            ->setLinkAttribute('class', 'dropdown-toggle')
                            ->setLinkAttribute('data-toggle', 'dropdown');
            $menu["Classement"]->setChildrenAttribute('class', 'dropdown-menu dropdown-menu-right');

            $menu["Classement"]->addChild('Joueurs', array("uri"=>'#'));//array('route' => 'maesbox_oginspector_ogiscan_index'));
            $menu["Classement"]->addChild('Alliances', array("uri"=>'#'));//array('route' => 'maesbox_oginspector_xtense_index'));
        }
        
        return $menu;
    }
    
    public function playerMenu($container)
    {
        $session = $container->get('session');
        $user = $container->get('security.context')->getToken()->getUser();
        $route = $container->get('request')->get('_route');
        $em = $container->get('doctrine.orm.entity_manager');
        
        $menu = $this->factory->createItem('root');
        
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        if($session->get('univers'))
        {
            $selected_univers = $em->getRepository('MaesboxOGInspectorBundle:Univers')->find($session->get('univers'));
            
            if($session->get('player'))
            {
                $selected_player = $em->getRepository('MaesboxOGInspectorBundle:Player')->find($session->get('player'));

                if($selected_player)
                {
                    $menu->addChild($selected_player->getName(), array('uri' => '#'.$selected_player->getName()))
                        ->setAttribute('class', 'dropdown')
                        ->setLabelAttributes(array('class' =>'fa fa-user'))
                        ->setLinkAttribute('class', 'dropdown-toggle')
                        ->setLinkAttribute('data-toggle', 'dropdown');
                    $menu[$selected_player->getName()]->setChildrenAttribute('class', 'dropdown-menu');

                    $menu[$selected_player->getName()]->addChild('Mon Profil',array('route' => 'maesbox_oginspector_profil'));

                    if($selected_player->getAlliance())
                    {
                        $menu[$selected_player->getName()]->addChild('Mon Alliance',array('route' => 'maesbox_oginspector_profil_alliance'));
                    }
                }
            }
        }
        
        return $menu;
    }
    
    public function chatMenu($container)
    {
        $em = $container->get('doctrine.orm.entity_manager');
        $session = $container->get('session');
        
        $menu = $this->factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        
        if($session->get('univers'))
        {
            $selected_univers = $em->getRepository('MaesboxOGInspectorBundle:Univers')->find($session->get('univers'));
            
            if($session->get('player') && $selected_univers)
            {
                $selected_player = $em->getRepository('MaesboxOGInspectorBundle:OGPlayer')->find($session->get('player'));

                if($selected_player)
                {
                    $menu->addChild('OGI Chat', array('uri' => '#'))
                        ->setAttribute('class', 'dropdown')
                        ->setLabelAttributes(array('class' =>'fa fa-comment'))
                        ->setLinkAttribute('class', 'dropdown-toggle ogi-chat-link')
                        ->setLinkAttribute('data-toggle', 'dropdown');
                    
                }
            }
        }
        
        return $menu;
    }
}