<?php

namespace Maesbox\OGInspectorBundle\Services;

use SimpleXMLElement;
use Buzz\Browser as Buzz;


class ApiOgame
{
    protected $buzz;
    
    /**
     * @param  $buzz
     */
    public function __construct(Buzz $buzz)
    {
        $this->setBuzz($buzz);
    }

    public function setBuzz(Buzz $buzz)
    {
        $this->buzz = $buzz;
        return $this;
    }
    
    /**
     * @return Buzz
     */
    public function getBuzz()
    {
        return $this->buzz;
    }
    

    /**
     * @param string $language
     * @return mixed array or false if error
     */
    public function getServerList($language)
    {
        $ogdata = $this->getBuzz()->get('http://s1-'.$language.'.ogame.gameforge.com/api/universes.xml');
        
        if($this->isValidXml($ogdata->getContent()))
        {
            $serverList = array();
            
            $data = new SimpleXMLElement($ogdata->getContent());

            foreach ($data->universe as $univers){
                $serverList[] = array(
                    "id" => (int) $univers['id'],
                    "domain" => (string) $univers['href']
                );
            }
            
            return array(
                'nb_items' => count($serverList),
                'items' => $serverList
            );
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @return mixed array or false if error
     */
    public function getServerInfos($language, $number)
    {
        $ogdata = $ogdata = $this->getBuzz()->get('http://s'.$number.'-'.$language.'.ogame.gameforge.com/api/serverData.xml');
        
        if($this->isValidXml($ogdata->getContent()))
        {
            $data = new SimpleXMLElement($ogdata->getContent());
        
            $server_infos = (array) $data;

            unset($server_infos["@attributes"]);
            
            return $server_infos;
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @return mixed array or false if error
     */
    public function getPlayerList($language, $number)
    {
        $ogdata = $this->getBuzz()->get('http://s'.$number.'-'.$language.'.ogame.gameforge.com/api/players.xml');
        
        if($this->isValidXml($ogdata->getContent()))
        {
            $playersList = array();
            
            $data = new SimpleXMLElement($ogdata->getContent());
            
            foreach ($data->player as $player){
                $aplayer = (array) $player;
                $playersList[] = $aplayer['@attributes'];
            }
            
            return array(
                'nb_items' => count($playersList),
                'items' => $playersList
            );
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @return mixed array or false if error
     */
    public function getAllianceList($language, $number)
    {
        $ogdata = $this->getBuzz()->get('http://s'.$number.'-'.$language.'.ogame.gameforge.com/api/alliances.xml');
        
        if($this->isValidXml($ogdata->getContent()))
        {
            $allianceList = array();
            
            $data = new SimpleXMLElement($ogdata->getContent());
            
            foreach ($data->alliance as $alliance){
                $aalliance = (array) $alliance;
                $allianceList[] = $aalliance['@attributes'];
            }
            
            return array(
                'nb_items' => count($allianceList),
                'items' => $allianceList
            );
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @param integer $id
     * @return mixed array or false if error
     */
    public function getPlayerPositions($language, $number, $id)
    {
        $ogdata = $this->getBuzz()->get('http://s'.$number.'-'.$language.'.ogame.gameforge.com/api/playerData.xml?id='.$id);
         
        if($this->isValidXml($ogdata->getContent()))
        {
            $data = new SimpleXMLElement($ogdata->getContent());
            $playerPositions = array();
            
            foreach ($data->planets->planet as $planet){
                $aplanet = (array) $planet;
                
                $position = $aplanet['@attributes'];
                
                if(isset($aplanet['moon'])){
                    $moon = (array) $aplanet['moon'];
                    $position['moon'] = $moon['@attributes'];
                }
                
                $playerPositions[] = $position;
            }
            
            return array(
                'nb_items' => count($playerPositions),
                'items' => $playerPositions
            );
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @param integer $category
     * @param integer $type
     * @return array
     */
    public function getScores($language, $number, $category, $type)
    {        
        $ogdata = $this->getBuzz()->get('http://s'.$number.'-'.$language.'.ogame.gameforge.com/api/highscore.xml?category='.$category.'&type='.$type);
        
        if($this->isValidXml($ogdata->getContent()))
        {
            $scores = array();
            
            $data = new SimpleXMLElement($ogdata->getContent());
            
            $date = (int) $data->attributes()->timestamp;
            $scores = array();
            
            foreach ($data->player as $player){
                $aplayer = (array) $player;
                                
                $scores[] = $aplayer['@attributes'];
            }
            
            foreach ($data->alliance as $alliance){
                $aaliance = (array) $alliance;
                                
                $scores[] = $aaliance['@attributes'];
            }
            
            return array(
                'nb_items' => count($scores),//$crawler->filter('highscore')->children()->count(),
                'date' => $date,//$crawler->filter('highscore')->attr('timestamp'),
                'items' => $scores//$scores
            );
        }
        
        return false;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @param integer $type
     * @return array
     */
    public function getPlayerScores($language, $number, $type)
    {
        $players_scores = $this->getScores($language, $number, 1, $type);
        
        return $players_scores;
    }
    
    /**
     * @param string $language
     * @param integer $number
     * @param integer $type
     * @return array
     */
    public function getAllianceScores($language, $number, $type)
    {
        $alliances_scores = $this->getScores($language, $number, 2, $type);
        
        return $alliances_scores;
    }
    
    /**
     * @param string $content
     * @return boolean
     */
    private function isValidXml($content)
    {
        $str = trim($content);
        if (empty($str)) {
            return false;
        }
        //html go to hell!
        if (stripos($str, '<!DOCTYPE html>') !== false || stripos($str, '<html>') !== false) {
            return false;
        }

        libxml_use_internal_errors(true);
        simplexml_load_string($str);
        $errors = libxml_get_errors();          
        libxml_clear_errors();  

        return empty($errors);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}