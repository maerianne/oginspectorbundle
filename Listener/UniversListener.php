<?php

namespace Maesbox\OGInspectorBundle\Listener;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class ImplementationListener
{
    private $manager;
    
    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        
        $baseclass = get_parent_class($controller[0]);
        
        if (!is_array($controller)) {
            return;
        }

        if ($baseclass && $baseclass == "Maesbox\OGInspectorBundle\Controller\BaseController" && $controller[0] instanceof $baseclass ) {

            $univers = $this->manager->getRepository("MaesboxOginspectorBundle:Univers")->findOneBy(
                        array(
                            "language" => $event->getRequest()->get('uni-lang'),
                            "number" => $event->getRequest()->get('uni-num')
                        ));

            if($univers){
                $controller->setUnivers($univers);
            }
            else
            {
                throw new AccessDeniedHttpException('You must create a site in database!');
            }
        }
    }
}