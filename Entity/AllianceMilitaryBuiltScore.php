<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Planete
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\AllianceMilitaryBuiltScoreRepository")
 */
class AllianceMilitaryBuiltScore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Alliance
     * @ORM\ManyToOne(targetEntity="Alliance", inversedBy="military_built_scores")
     * @ORM\JoinColumn(name="alliance_id", referencedColumnName="id")
     */
    protected $alliance;
    
    /**
     * @var Datetime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var integer
     * @ORM\Column(name="score", type="integer")
     */
    protected $score;
    
    /**
     * @var integer
     * @ORM\Column(name="rank", type="integer")
     */
    protected $rank;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return AllianceMilitaryBuiltScore
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return AllianceMilitaryBuiltScore
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return AllianceMilitaryBuiltScore
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set alliance
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Alliance $alliance
     *
     * @return AllianceMilitaryBuiltScore
     */
    public function setAlliance(\Maesbox\OGInspectorBundle\Entity\Alliance $alliance = null)
    {
        $this->alliance = $alliance;

        return $this;
    }

    /**
     * Get alliance
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Alliance
     */
    public function getAlliance()
    {
        return $this->alliance;
    }
}
