<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Maesbox\OGInspectorBundle\Model\UserInterface;

/**
 * Player
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var OGIUser
     * @ORM\ManyToOne(targetEntity="UserInterface", inversedBy="players")
     */
    protected $user;
    
    /**
     * @var integer
     * @ORM\Column(name="ogid", type="integer", nullable=true)
     */
    protected $ogid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="vacances", type="boolean")
     */
    protected $vacances = false;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="inactif", type="boolean")
     */
    protected $inactif = false;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="long_inactif", type="boolean")
     */
    protected $long_inactif = false;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="banned", type="boolean")
     */
    protected $banned = false;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="deleted", type="boolean")
     */
    protected $deleted = false;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="ogadmin", type="boolean")
     */
    protected $ogadmin = false;
    
    /**
     * @var Univers
     * @ORM\ManyToOne(targetEntity="Univers", inversedBy="players")
     * @ORM\JoinColumn(name="univers_id", referencedColumnName="id", nullable=true)
     */
    protected $univers;
    
    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="Alliance", inversedBy="players")
     * @ORM\JoinColumn(name="alliance_id", referencedColumnName="id", nullable=true)
     */
    protected $alliance;
    
    /**
     * @ORM\OneToMany(targetEntity="Planete", mappedBy="player")
     **/
    protected $planetes;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerGeneralScore", mappedBy="player")
     **/
    protected $general_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerEconomicScore", mappedBy="player")
     **/
    protected $economic_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerResearchScore", mappedBy="player")
     **/
    protected $research_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerMilitaryScore", mappedBy="player")
     **/
    protected $military_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerMilitaryBuiltScore", mappedBy="player")
     **/
    protected $military_built_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerMilitaryDestroyedScore", mappedBy="player")
     **/
    protected $military_destroyed_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerMilitaryLostScore", mappedBy="player")
     **/
    protected $military_lost_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="PlayerHonorScore", mappedBy="player")
     **/
    protected $honor_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="SpyReport", mappedBy="player")
     **/
    protected $spy_reports;
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
    
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planetes = new ArrayCollection();
        $this->general_scores = new ArrayCollection();
        $this->economic_scores = new ArrayCollection();
        $this->research_scores = new ArrayCollection();
        $this->military_built_scores = new ArrayCollection();
        $this->military_destroyed_scores = new ArrayCollection();
        $this->military_lost_scores = new ArrayCollection();
        $this->honor_scores = new ArrayCollection();
        $this->spy_reports = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ogid
     *
     * @param integer $ogid
     *
     * @return Player
     */
    public function setOgid($ogid)
    {
        $this->ogid = $ogid;

        return $this;
    }

    /**
     * Get ogid
     *
     * @return integer
     */
    public function getOgid()
    {
        return $this->ogid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set vacances
     *
     * @param boolean $vacances
     *
     * @return Player
     */
    public function setVacances($vacances)
    {
        $this->vacances = $vacances;

        return $this;
    }

    /**
     * Get vacances
     *
     * @return boolean
     */
    public function getVacances()
    {
        return $this->vacances;
    }

    /**
     * Set inactif
     *
     * @param boolean $inactif
     *
     * @return Player
     */
    public function setInactif($inactif)
    {
        $this->inactif = $inactif;

        return $this;
    }

    /**
     * Get inactif
     *
     * @return boolean
     */
    public function getInactif()
    {
        return $this->inactif;
    }

    /**
     * Set longInactif
     *
     * @param boolean $longInactif
     *
     * @return Player
     */
    public function setLongInactif($longInactif)
    {
        $this->long_inactif = $longInactif;

        return $this;
    }

    /**
     * Get longInactif
     *
     * @return boolean
     */
    public function getLongInactif()
    {
        return $this->long_inactif;
    }

    /**
     * Set banned
     *
     * @param boolean $banned
     *
     * @return Player
     */
    public function setBanned($banned)
    {
        $this->banned = $banned;

        return $this;
    }

    /**
     * Get banned
     *
     * @return boolean
     */
    public function getBanned()
    {
        return $this->banned;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Player
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set ogadmin
     *
     * @param boolean $ogadmin
     *
     * @return Player
     */
    public function setOgadmin($ogadmin)
    {
        $this->ogadmin = $ogadmin;

        return $this;
    }

    /**
     * Get ogadmin
     *
     * @return boolean
     */
    public function getOgadmin()
    {
        return $this->ogadmin;
    }

    /**
     * Set user
     *
     * @param UserInterface $user
     *
     * @return Player
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Maesbox\OGInspectorBundle\Interface\UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set univers
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Univers $univers
     *
     * @return Player
     */
    public function setUnivers(\Maesbox\OGInspectorBundle\Entity\Univers $univers = null)
    {
        $this->univers = $univers;

        return $this;
    }

    /**
     * Get univers
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Univers
     */
    public function getUnivers()
    {
        return $this->univers;
    }

    /**
     * Set alliance
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Alliance $alliance
     *
     * @return Player
     */
    public function setAlliance(\Maesbox\OGInspectorBundle\Entity\Alliance $alliance = null)
    {
        $this->alliance = $alliance;

        return $this;
    }

    /**
     * Get alliance
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Alliance
     */
    public function getAlliance()
    {
        return $this->alliance;
    }

    /**
     * Add planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     *
     * @return Player
     */
    public function addPlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete)
    {
        $this->planetes[] = $planete;

        return $this;
    }

    /**
     * Remove planete
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Planete $planete
     */
    public function removePlanete(\Maesbox\OGInspectorBundle\Entity\Planete $planete)
    {
        $this->planetes->removeElement($planete);
    }

    /**
     * Get planetes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanetes()
    {
        return $this->planetes;
    }

    /**
     * Add generalScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerGeneralScore $generalScore
     *
     * @return Player
     */
    public function addGeneralScore(\Maesbox\OGInspectorBundle\Entity\PlayerGeneralScore $generalScore)
    {
        $this->general_scores[] = $generalScore;

        return $this;
    }

    /**
     * Remove generalScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerGeneralScore $generalScore
     */
    public function removeGeneralScore(\Maesbox\OGInspectorBundle\Entity\PlayerGeneralScore $generalScore)
    {
        $this->general_scores->removeElement($generalScore);
    }

    /**
     * Get generalScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneralScores()
    {
        return $this->general_scores;
    }

    /**
     * Add economicScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerEconomicScore $economicScore
     *
     * @return Player
     */
    public function addEconomicScore(\Maesbox\OGInspectorBundle\Entity\PlayerEconomicScore $economicScore)
    {
        $this->economic_scores[] = $economicScore;

        return $this;
    }

    /**
     * Remove economicScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerEconomicScore $economicScore
     */
    public function removeEconomicScore(\Maesbox\OGInspectorBundle\Entity\PlayerEconomicScore $economicScore)
    {
        $this->economic_scores->removeElement($economicScore);
    }

    /**
     * Get economicScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEconomicScores()
    {
        return $this->economic_scores;
    }

    /**
     * Add researchScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerResearchScore $researchScore
     *
     * @return Player
     */
    public function addResearchScore(\Maesbox\OGInspectorBundle\Entity\PlayerResearchScore $researchScore)
    {
        $this->research_scores[] = $researchScore;

        return $this;
    }

    /**
     * Remove researchScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerResearchScore $researchScore
     */
    public function removeResearchScore(\Maesbox\OGInspectorBundle\Entity\PlayerResearchScore $researchScore)
    {
        $this->research_scores->removeElement($researchScore);
    }

    /**
     * Get researchScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResearchScores()
    {
        return $this->research_scores;
    }

    /**
     * Add militaryScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryScore $militaryScore
     *
     * @return Player
     */
    public function addMilitaryScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryScore $militaryScore)
    {
        $this->military_scores[] = $militaryScore;

        return $this;
    }

    /**
     * Remove militaryScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryScore $militaryScore
     */
    public function removeMilitaryScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryScore $militaryScore)
    {
        $this->military_scores->removeElement($militaryScore);
    }

    /**
     * Get militaryScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryScores()
    {
        return $this->military_scores;
    }

    /**
     * Add militaryBuiltScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryBuiltScore $militaryBuiltScore
     *
     * @return Player
     */
    public function addMilitaryBuiltScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryBuiltScore $militaryBuiltScore)
    {
        $this->military_built_scores[] = $militaryBuiltScore;

        return $this;
    }

    /**
     * Remove militaryBuiltScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryBuiltScore $militaryBuiltScore
     */
    public function removeMilitaryBuiltScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryBuiltScore $militaryBuiltScore)
    {
        $this->military_built_scores->removeElement($militaryBuiltScore);
    }

    /**
     * Get militaryBuiltScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryBuiltScores()
    {
        return $this->military_built_scores;
    }

    /**
     * Add militaryDestroyedScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryDestroyedScore $militaryDestroyedScore
     *
     * @return Player
     */
    public function addMilitaryDestroyedScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryDestroyedScore $militaryDestroyedScore)
    {
        $this->military_destroyed_scores[] = $militaryDestroyedScore;

        return $this;
    }

    /**
     * Remove militaryDestroyedScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryDestroyedScore $militaryDestroyedScore
     */
    public function removeMilitaryDestroyedScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryDestroyedScore $militaryDestroyedScore)
    {
        $this->military_destroyed_scores->removeElement($militaryDestroyedScore);
    }

    /**
     * Get militaryDestroyedScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryDestroyedScores()
    {
        return $this->military_destroyed_scores;
    }

    /**
     * Add militaryLostScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryLostScore $militaryLostScore
     *
     * @return Player
     */
    public function addMilitaryLostScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryLostScore $militaryLostScore)
    {
        $this->military_lost_scores[] = $militaryLostScore;

        return $this;
    }

    /**
     * Remove militaryLostScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerMilitaryLostScore $militaryLostScore
     */
    public function removeMilitaryLostScore(\Maesbox\OGInspectorBundle\Entity\PlayerMilitaryLostScore $militaryLostScore)
    {
        $this->military_lost_scores->removeElement($militaryLostScore);
    }

    /**
     * Get militaryLostScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryLostScores()
    {
        return $this->military_lost_scores;
    }

    /**
     * Add honorScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerHonorScore $honorScore
     *
     * @return Player
     */
    public function addHonorScore(\Maesbox\OGInspectorBundle\Entity\PlayerHonorScore $honorScore)
    {
        $this->honor_scores[] = $honorScore;

        return $this;
    }

    /**
     * Remove honorScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\PlayerHonorScore $honorScore
     */
    public function removeHonorScore(\Maesbox\OGInspectorBundle\Entity\PlayerHonorScore $honorScore)
    {
        $this->honor_scores->removeElement($honorScore);
    }

    /**
     * Get honorScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHonorScores()
    {
        return $this->honor_scores;
    }

    /**
     * Add spyReport
     *
     * @param \Maesbox\OGInspectorBundle\Entity\SpyReport $spyReport
     *
     * @return Player
     */
    public function addSpyReport(\Maesbox\OGInspectorBundle\Entity\SpyReport $spyReport)
    {
        $this->spy_reports[] = $spyReport;

        return $this;
    }

    /**
     * Remove spyReport
     *
     * @param \Maesbox\OGInspectorBundle\Entity\SpyReport $spyReport
     */
    public function removeSpyReport(\Maesbox\OGInspectorBundle\Entity\SpyReport $spyReport)
    {
        $this->spy_reports->removeElement($spyReport);
    }

    /**
     * Get spyReports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpyReports()
    {
        return $this->spy_reports;
    }
}
