<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Planete
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\PlayerResearchScoreRepository")
 */
class PlayerResearchScore
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="research_scores")
     * @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     */
    protected $player;
    
    /**
     * @var Datetime
     * @ORM\Column(name="date", type="datetime")
     */
    protected $date;

    /**
     * @var integer
     * @ORM\Column(name="score", type="integer")
     */
    protected $score;
    
    /**
     * @var integer
     * @ORM\Column(name="rank", type="integer")
     */
    protected $rank;
        
    
    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return PlayerResearchScore
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return PlayerResearchScore
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return PlayerResearchScore
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Set player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     *
     * @return PlayerResearchScore
     */
    public function setPlayer(\Maesbox\OGInspectorBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
