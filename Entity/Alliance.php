<?php

namespace Maesbox\OGInspectorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Alliance
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Maesbox\OGInspectorBundle\Repository\AllianceRepository")
 */
class Alliance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @var integer
     * @ORM\Column(name="ogid", type="integer", nullable=true)
     */
    protected $ogid;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", nullable=true)
     */
    protected $tag;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", nullable=true)
     */
    protected $homepage;
    
    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", nullable=true)
     */
    protected $logo;
    
    /**
     * @var boolean
     * 
     * @ORM\Column(name="deleted", type="boolean")
     */
    protected $deleted = false;
    
    /**
     * @var Univers
     * @ORM\ManyToOne(targetEntity="Univers", inversedBy="alliances")
     * @ORM\JoinColumn(name="univers_id", referencedColumnName="id", nullable=true)
     */
    protected $univers;
    
    /**
     * @ORM\OneToMany(targetEntity="Player", mappedBy="alliance")
     **/
    protected $players;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceGeneralScore", mappedBy="alliance")
     **/
    protected $general_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceEconomicScore", mappedBy="alliance")
     **/
    protected $economic_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceResearchScore", mappedBy="alliance")
     **/
    protected $research_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceMilitaryScore", mappedBy="alliance")
     **/
    protected $military_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceMilitaryBuiltScore", mappedBy="alliance")
     **/
    protected $military_built_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceMilitaryDestroyedScore", mappedBy="alliance")
     **/
    protected $military_destroyed_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceMilitaryLostScore", mappedBy="alliance")
     **/
    protected $military_lost_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceHonorScore", mappedBy="alliance")
     **/
    protected $honor_scores;
    
    /**
     * @ORM\OneToMany(targetEntity="AllianceNbPlayer", mappedBy="alliance")
     **/
    protected $nb_players;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->general_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->economic_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->research_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->military_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->military_built_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->military_destroyed_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->military_lost_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->honor_scores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nb_players = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ogid
     *
     * @param integer $ogid
     *
     * @return Alliance
     */
    public function setOgid($ogid)
    {
        $this->ogid = $ogid;

        return $this;
    }

    /**
     * Get ogid
     *
     * @return integer
     */
    public function getOgid()
    {
        return $this->ogid;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return Alliance
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Alliance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set homepage
     *
     * @param string $homepage
     *
     * @return Alliance
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;

        return $this;
    }

    /**
     * Get homepage
     *
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Alliance
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return Alliance
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set univers
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Univers $univers
     *
     * @return Alliance
     */
    public function setUnivers(\Maesbox\OGInspectorBundle\Entity\Univers $univers = null)
    {
        $this->univers = $univers;

        return $this;
    }

    /**
     * Get univers
     *
     * @return \Maesbox\OGInspectorBundle\Entity\Univers
     */
    public function getUnivers()
    {
        return $this->univers;
    }

    /**
     * Add player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     *
     * @return Alliance
     */
    public function addPlayer(\Maesbox\OGInspectorBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \Maesbox\OGInspectorBundle\Entity\Player $player
     */
    public function removePlayer(\Maesbox\OGInspectorBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Add generalScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceGeneralScore $generalScore
     *
     * @return Alliance
     */
    public function addGeneralScore(\Maesbox\OGInspectorBundle\Entity\AllianceGeneralScore $generalScore)
    {
        $this->general_scores[] = $generalScore;

        return $this;
    }

    /**
     * Remove generalScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceGeneralScore $generalScore
     */
    public function removeGeneralScore(\Maesbox\OGInspectorBundle\Entity\AllianceGeneralScore $generalScore)
    {
        $this->general_scores->removeElement($generalScore);
    }

    /**
     * Get generalScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneralScores()
    {
        return $this->general_scores;
    }

    /**
     * Add economicScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceEconomicScore $economicScore
     *
     * @return Alliance
     */
    public function addEconomicScore(\Maesbox\OGInspectorBundle\Entity\AllianceEconomicScore $economicScore)
    {
        $this->economic_scores[] = $economicScore;

        return $this;
    }

    /**
     * Remove economicScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceEconomicScore $economicScore
     */
    public function removeEconomicScore(\Maesbox\OGInspectorBundle\Entity\AllianceEconomicScore $economicScore)
    {
        $this->economic_scores->removeElement($economicScore);
    }

    /**
     * Get economicScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEconomicScores()
    {
        return $this->economic_scores;
    }

    /**
     * Add researchScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceResearchScore $researchScore
     *
     * @return Alliance
     */
    public function addResearchScore(\Maesbox\OGInspectorBundle\Entity\AllianceResearchScore $researchScore)
    {
        $this->research_scores[] = $researchScore;

        return $this;
    }

    /**
     * Remove researchScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceResearchScore $researchScore
     */
    public function removeResearchScore(\Maesbox\OGInspectorBundle\Entity\AllianceResearchScore $researchScore)
    {
        $this->research_scores->removeElement($researchScore);
    }

    /**
     * Get researchScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResearchScores()
    {
        return $this->research_scores;
    }

    /**
     * Add militaryScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryScore $militaryScore
     *
     * @return Alliance
     */
    public function addMilitaryScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryScore $militaryScore)
    {
        $this->military_scores[] = $militaryScore;

        return $this;
    }

    /**
     * Remove militaryScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryScore $militaryScore
     */
    public function removeMilitaryScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryScore $militaryScore)
    {
        $this->military_scores->removeElement($militaryScore);
    }

    /**
     * Get militaryScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryScores()
    {
        return $this->military_scores;
    }

    /**
     * Add militaryBuiltScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryBuiltScore $militaryBuiltScore
     *
     * @return Alliance
     */
    public function addMilitaryBuiltScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryBuiltScore $militaryBuiltScore)
    {
        $this->military_built_scores[] = $militaryBuiltScore;

        return $this;
    }

    /**
     * Remove militaryBuiltScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryBuiltScore $militaryBuiltScore
     */
    public function removeMilitaryBuiltScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryBuiltScore $militaryBuiltScore)
    {
        $this->military_built_scores->removeElement($militaryBuiltScore);
    }

    /**
     * Get militaryBuiltScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryBuiltScores()
    {
        return $this->military_built_scores;
    }

    /**
     * Add militaryDestroyedScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryDestroyedScore $militaryDestroyedScore
     *
     * @return Alliance
     */
    public function addMilitaryDestroyedScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryDestroyedScore $militaryDestroyedScore)
    {
        $this->military_destroyed_scores[] = $militaryDestroyedScore;

        return $this;
    }

    /**
     * Remove militaryDestroyedScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryDestroyedScore $militaryDestroyedScore
     */
    public function removeMilitaryDestroyedScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryDestroyedScore $militaryDestroyedScore)
    {
        $this->military_destroyed_scores->removeElement($militaryDestroyedScore);
    }

    /**
     * Get militaryDestroyedScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryDestroyedScores()
    {
        return $this->military_destroyed_scores;
    }

    /**
     * Add militaryLostScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryLostScore $militaryLostScore
     *
     * @return Alliance
     */
    public function addMilitaryLostScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryLostScore $militaryLostScore)
    {
        $this->military_lost_scores[] = $militaryLostScore;

        return $this;
    }

    /**
     * Remove militaryLostScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceMilitaryLostScore $militaryLostScore
     */
    public function removeMilitaryLostScore(\Maesbox\OGInspectorBundle\Entity\AllianceMilitaryLostScore $militaryLostScore)
    {
        $this->military_lost_scores->removeElement($militaryLostScore);
    }

    /**
     * Get militaryLostScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMilitaryLostScores()
    {
        return $this->military_lost_scores;
    }

    /**
     * Add honorScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceHonorScore $honorScore
     *
     * @return Alliance
     */
    public function addHonorScore(\Maesbox\OGInspectorBundle\Entity\AllianceHonorScore $honorScore)
    {
        $this->honor_scores[] = $honorScore;

        return $this;
    }

    /**
     * Remove honorScore
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceHonorScore $honorScore
     */
    public function removeHonorScore(\Maesbox\OGInspectorBundle\Entity\AllianceHonorScore $honorScore)
    {
        $this->honor_scores->removeElement($honorScore);
    }

    /**
     * Get honorScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHonorScores()
    {
        return $this->honor_scores;
    }

    /**
     * Add nbPlayer
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceNbPlayer $nbPlayer
     *
     * @return Alliance
     */
    public function addNbPlayer(\Maesbox\OGInspectorBundle\Entity\AllianceNbPlayer $nbPlayer)
    {
        $this->nb_players[] = $nbPlayer;

        return $this;
    }

    /**
     * Remove nbPlayer
     *
     * @param \Maesbox\OGInspectorBundle\Entity\AllianceNbPlayer $nbPlayer
     */
    public function removeNbPlayer(\Maesbox\OGInspectorBundle\Entity\AllianceNbPlayer $nbPlayer)
    {
        $this->nb_players->removeElement($nbPlayer);
    }

    /**
     * Get nbPlayers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNbPlayers()
    {
        return $this->nb_players;
    }
}
