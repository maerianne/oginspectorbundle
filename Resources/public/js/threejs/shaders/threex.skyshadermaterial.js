var THREEx	= THREEx	|| {};

THREEx.SkyShaderMaterial	= function(opts){
	opts	= opts	|| {};
	opts.vertexShader	= opts.vertexShader	|| THREEx.SkyShaderMaterial.vertexShader;
	opts.fragmentShader	= opts.fragmentShader	|| THREEx.SkyShaderMaterial.fragmentShader;
	opts.uniforms		=  { "tCube": { type: "t", value: null }, "tFlip": { type: "f", value: -1 } };
	var material	= new THREE.ShaderMaterial(opts);
	return material;
};

THREEx.SkyShaderMaterial.fragmentShader = [

    "uniform samplerCube tCube;",
    "uniform float tFlip;",

    "varying vec3 vWorldPosition;",

    THREE.ShaderChunk[ "logdepthbuf_pars_fragment" ],

    "void main() {",

    "   gl_FragColor = textureCube( tCube, vec3( tFlip * vWorldPosition.x, vWorldPosition.yz ) );",

        THREE.ShaderChunk[ "logdepthbuf_fragment" ],

    "}"

].join("\n");


THREEx.SkyShaderMaterial.vertexShader = [

    "varying vec3 vWorldPosition;",

    THREE.ShaderChunk[ "logdepthbuf_pars_vertex" ],

    "void main() {",

    "   vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
    "   vWorldPosition = worldPosition.xyz;",

    "   gl_Position = projectionMatrix * modelViewMatrix * vec4( position + cameraPosition, 1.0 );",

        THREE.ShaderChunk[ "logdepthbuf_vertex" ],

    "}"

].join("\n");
