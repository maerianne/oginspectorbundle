
Skybox = function(imagePath){
    this.imagePath = imagePath;
    //this.loader = loader;
    
    this.cubeMap = new THREE.CubeTexture( [] );
    
    this.cubeMap.format = THREE.RGBFormat;
    this.cubeMap.flipY = false;
    
    this.cubeShader = THREEx.SkyShaderMaterial();
    this.cubeShader.uniforms['tCube'].value = this.cubeMap;
                                
    this.skyBoxMaterial = new THREE.ShaderMaterial( {
        fragmentShader: this.cubeShader.fragmentShader,
        vertexShader: this.cubeShader.vertexShader,
        uniforms: this.cubeShader.uniforms,
        depthWrite: true,
        side: THREE.BackSide
    });
        
    this.mesh = new THREE.Mesh(
        new THREE.BoxGeometry(1000000, 1000000, 1000000 ),
        this.skyBoxMaterial
    );
};

Skybox.prototype = {
    constructor: Skybox,
      
    apply: function(image){
        var getSide = function ( x, y ) {
            var size = 1024;
            var canvas = document.createElement( 'canvas' );
            canvas.width = size;
            canvas.height = size;
            var context = canvas.getContext( '2d' );
            context.drawImage( image, - x * size, - y * size );
            return canvas;
        };

        this.cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
        this.cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
        this.cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
        this.cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
        this.cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
        this.cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
        this.cubeMap.needsUpdate = true;
    },
    
    getRessourcesPaths: function(){
        return {image: [ this.imagePath ]};
    },
    
    getMesh: function(){
        
        
        return this.mesh;
    }
};

/*
initSkybox: function(){
        var cubeMap = new THREE.CubeTexture( [] );
	cubeMap.format = THREE.RGBFormat;
	cubeMap.flipY = false;
	var loader = new THREE.ImageLoader();
	loader.load( '/bundles/maesboxoginspector/models/textures/skybox.jpg', function ( image ) {
                var getSide = function ( x, y ) {
                    var size = 1024;
                    var canvas = document.createElement( 'canvas' );
                    canvas.width = size;
                    canvas.height = size;
                    var context = canvas.getContext( '2d' );
                    context.drawImage( image, - x * size, - y * size );
                    return canvas;
		};
                
		cubeMap.images[ 0 ] = getSide( 2, 1 ); // px
		cubeMap.images[ 1 ] = getSide( 0, 1 ); // nx
		cubeMap.images[ 2 ] = getSide( 1, 0 ); // py
		cubeMap.images[ 3 ] = getSide( 1, 2 ); // ny
		cubeMap.images[ 4 ] = getSide( 1, 1 ); // pz
		cubeMap.images[ 5 ] = getSide( 3, 1 ); // nz
		cubeMap.needsUpdate = true;
	} );
	
        var cubeShader = THREEx.SkyShaderMaterial();
	cubeShader.uniforms['tCube'].value = cubeMap;
                                
                                
				var skyBoxMaterial = new THREE.ShaderMaterial( {
					fragmentShader: cubeShader.fragmentShader,
					vertexShader: cubeShader.vertexShader,
					uniforms: cubeShader.uniforms,
					depthWrite: true,
					side: THREE.BackSide
				});
	this.skyBox = new THREE.Mesh(
            new THREE.BoxGeometry(1000000, 1000000, 1000000 ),
					skyBoxMaterial
				);
				
	this.scene.add( this.skyBox );
        
    },*/


