function Planete( parameters ){
    this.system = null;
    this.moon = null;
    this.ring = null;
    
    this.orbit = parameters.orbit || 0;
    
    this.paths.image = parameters.image || null;
    this.paths.bump = parameters.bump || parameters.image;
    
    this.hasRing = false;
    
    if(typeof parameters.ring !== "undefined"){
        this.hasRing = true;
        this.paths.imageRing = parameters.ring.image || null;
        this.paths.bumpRing = parameters.ring.bump || parameters.ring.image ; 
    }
    
    
    
    this.textures.image = null;
    this.textures.bump = null;
    this.textures.imageRing = null;
    this.textures.bumpRing = null;
    
    
};

Planete.prototype = {
    
    constructor: Planete,
    
    load: function(loader) {
        var context = this;
        
        
    },
    
    setOrbit: function(orbit)
    {
        this.orbit = orbit;
    },
    
    setPosition: function(x,y,z){
        
        this.sphere.position.set(x , y ,z );
        
        if(this.hasRing) {
            this.ring.position.set(x , y ,z );
        }
    },
    
    getPosition: function(){
        return this.sphere.position;
    },
    
    rotate: function(){
        /*
        this.sphere.rotation.x += 0.005;
        this.sphere.rotation.y += 0.005;
        this.sphere.rotation.z += 0.005;
        */
    },
    
    animate: function(){
        var timer = Date.now() * 0.00005;
        
        this.rotate();
        
        if(this.moon !== null) {
            this.moon.animate();
        }
        
        if(this.system !== null) {
            this.setPosition( this.system.getPosition().x + Math.cos( timer ) * ( 600 + 200 * this.orbit), this.system.getPosition().y +Math.sin( timer ) * ( 600 + 200 * this.orbit), this.system.getPosition().z );
        }
    }
};

/*
    init: function(){
        this.geometry = new THREE.SphereGeometry( Math.floor(Math.random() * 15) + 10, 50, 50 );
        
        var rand = Math.floor(Math.random() * 14);
        //rand= 14;
        console.log(rand);
        //OK
        if(rand == 0){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/plutomap1k.jpg" );
            this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/plutobump1k.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.bumpTexture.minFilter = THREE.LinearFilter;
            this.bumpTexture.anisotropy = 16;
        }
        //OK
        if(rand == 1){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/marsmap1k.jpg" );
            this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/marsbump1k.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.bumpTexture.minFilter = THREE.LinearFilter;
            this.bumpTexture.anisotropy = 16;
        }
        //OK 
        if(rand == 2){
            this.imgTexture = this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/mercurymap.jpg" );
            this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/mercurybump.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;        
            this.bumpTexture.wrapS = this.bumpTexture.wrapT = THREE.RepeatWrapping;
            this.bumpTexture.anisotropy = 16;
        }
        //OK
        if(rand == 3){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/mar1kuu2_jpg.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 4){
            this.imgTexture = this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/venusmap.jpg" );
            this.bumpTexture  = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/venusbump.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;        
            this.bumpTexture.wrapS = this.bumpTexture.wrapT = THREE.RepeatWrapping;
            this.bumpTexture.anisotropy = 16;
        }
        //OK
        if(rand == 5){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/planet_Klendathu1200.png" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;        
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 6){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/uranusmap.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 7){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/urloth.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 8){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/urlplanete.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 9){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/saturnmap.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        //OK
        if(rand == 10){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/planet_y.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        
        if(rand == 11){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/neptunemap.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        
        if(rand == 12){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/MarsNew3.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        
        if(rand == 13){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/Mars-Terraformed.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.RepeatWrapping;
            this.imgTexture.anisotropy = 16;
            this.bumpTexture = this.imgTexture ;
        }
        
        if(rand == 14){
            this.imgTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/Planet_Rust_Texture_by_DemosthenesVoice.jpg" );
            this.imgTexture.wrapS = this.imgTexture.wrapT = THREE.ClampToEdgeWrapping;
            this.imgTexture.minFilter = THREE.LinearFilter;
            this.bumpTexture = this.imgTexture ;
        }
        
        this.material = new THREE.MeshPhongMaterial( { map: this.imgTexture, bumpMap: this.bumpTexture, bumpScale: 1, color: 0xffffff, specular: 0x333333, shininess: 0, shading: THREE.SmoothShading } ) 
        
        this.sphere = new THREE.Mesh( this.geometry, this.material );
        
        if(this.hasRing)
        {
            this.initRing();
        }
        
        if(this.system  !== null)
        {
            this.setPosition(600 + this.system.getPosition().x + 200 * this.orbit, 600 + this.system.getPosition().y + 200 * this.orbit , this.system.getPosition().z);
        }
        else
        {
            this.setPosition( 0, 0, 0);
        }
        
        this.sphere.castShadow = true;
	this.sphere.receiveShadow = true;
    },
    
    initRing: function(){
        var rand = Math.floor(Math.random() * 2);
        var geometry = new THREE.RadialRingGeometry( 30, 40, 100,0 , Math.PI * 2 );
        
        if(rand == 0){
            this.ringTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/saturnringcolor.jpg" );
            this.ringTexture.anisotropy = 32;
            this.ringTexture.minFilter = THREE.LinearFilter;
            this.ringBumpTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/saturnringpattern.gif" );
            this.ringBumpTexture.anisotropy = 32;
            this.ringBumpTexture.minFilter = THREE.LinearFilter;
        }
        
        if(rand >= 1){
            this.ringTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/uranusringcolour.jpg" );
            this.ringTexture.anisotropy = 32;
            this.ringTexture.minFilter = THREE.LinearFilter;
            this.ringBumpTexture = THREE.ImageUtils.loadTexture( "/bundles/maesboxoginspector/models/textures/uranusringtrans.gif" );
            this.ringBumpTexture.anisotropy = 32;
            this.ringBumpTexture.minFilter = THREE.LinearFilter;
        }
        
        
        var material = new THREE.MeshPhongMaterial( { map: this.ringTexture, bumpMap: this.ringBumpTexture, transparent: true, bumpScale: 1, opacity: 0.6, specular: 0x333333, shininess: 0, shading: THREE.SmoothShading, side: THREE.DoubleSide } );
        this.ring = new THREE.Mesh( geometry, material );
    },
    
    addToScene: function(scene){
        this.init();
        scene.add(this.sphere);
        if(this.hasRing)
        {
            scene.add(this.ring);
        }
        
        if(this.moon !== null)
        {
            this.moon.addToScene(scene);
        }
    },
    
    setSystem: function(system){
        this.system = system;
    },
    
    addMoon: function(){
        this.moon = new Lune();
        this.moon.setPlanete(this);
    },
    
    setOrbit: function(orbit)
    {
        this.orbit = orbit;
    },
    
    setPosition: function(x,y,z){
        this.sphere.position.set(x , y ,z );
        if(this.hasRing)
        {
            this.ring.position.set(x , y ,z );
        }
    },
    
    getPosition: function(){
        return this.sphere.position;
    },
    
    animate: function(){
        var timer = Date.now() * 0.00005;
        
        this.sphere.rotation.x += 0.005;
        this.sphere.rotation.y += 0.005;
        this.sphere.rotation.z += 0.005;
        
        if(this.moon !== null)
        {
            this.moon.animate();
        }
        
        if(this.system !== null)
        {
            this.setPosition( this.system.getPosition().x + Math.cos( timer ) * ( 600 + 200 * this.orbit), this.system.getPosition().y +Math.sin( timer ) * ( 600 + 200 * this.orbit), this.system.getPosition().z );
        }
    }
};
*/