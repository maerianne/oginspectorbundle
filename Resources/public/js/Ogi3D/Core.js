
function Core(container){
    this.container = container;
    
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
//        this.scene.fog =
    this.stats = new Stats();

    this.scenes = [];
    
    this.initLoader();
};

Core.prototype = {
    constructor: Core(),
    
    init: function(){
        this.renderer.setPixelRatio( window.devicePixelRatio );
        this.renderer.setSize( this.container.width(), this.container.height() );
        this.renderer.setClearColor( 0x777777 );
        this.renderer.autoClear = true;
        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapType = THREE.PCFShadowMap;
        this.container.append( this.renderer.domElement );
        
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.bottom = '0px';
        this.container.append( this.stats.domElement );
    },
    
    initLoader: function(){
        this.loader.manager = new THREE.LoadingManager();
        this.loader.manager.onProgress = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        this.loader.manager.onLoad = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        this.loader.manager.onError = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };
        
        this.loader.image = new THREE.ImageLoader(this.loader.manager);
        this.loader.texture = new THREE.TextureLoader( this.loader.manager);
        
//        loadingManager: ,
//    objLoader: new THREEx.UniversalLoader(  ),//this.loadingManager
//    //colladaLoader: THREE.ColladaLoader( this.loadingManager ),
//    textureloader: 
    },
    
    addScene: function(scene){
        if(this.scenes.length === 0){
            this.currentScene = scene;
        }
        this.scenes.push(scene);
    },
    
    getScenesNames: function(){
        var names = [];
        this.scenes.forEach(function(item){
            names.push(item.name);
        });
    },
    
    animate: function() {
        var context = this;
        
        if(typeof this.scene !== "undefined"){
            window.requestAnimationFrame( function(){ 
                context.animate(); 
            });
            
            this.stats.update();
            this.render();
        }
    },

    render: function() {
        
        this.renderer.render( this.currentScene.scene, this.currentScene.camera );//this.renderer.render( this.skyscene, this.skycamera );
    }
};