
$(document).ready(function(){
    
    
    
    $('.sidebar').on('hide',function(){
        $('body').css({'padding-left': "0px"});
        $('.sidebar-toggle').offset({left: 0});
        $('.sidebar-toggle > span').attr({'class': "glyphicon glyphicon-chevron-right"});
    });
    
    $('.sidebar').on('show',function(){
        if($('.sidebar-toggle').is(':hidden')){
            $('body').css({'padding-left': "250px"});
        }
        else
        {
            $('.sidebar-toggle').offset({left: 250});
        }
        $('.sidebar-toggle > span').attr({'class': "glyphicon glyphicon-chevron-left"});
    });
    
    $(window).resize(function(){
        if($('.sidebar-toggle').is(':hidden')){
            $('.sidebar').show();
        }
        else
        {
            $('.sidebar').hide();
        }
    });
    
    $('.sidebar-toggle').click(function(){
        if($('.sidebar').is(':hidden')){
            $('.sidebar').show();
        } 
        else
        {
            $('.sidebar').hide();
        }  
    });
    
    $(window).resize();
});