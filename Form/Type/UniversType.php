<?php

namespace Maesbox\OGInspectorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;

class UniversType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'language', 
                CountryType::class, 
                array(
                    'choices' => array(
                        "french" => "fr",
                        "english" => "en",
                        "german" => "de"
                    )
                ))
            ->add('number', 'integer')
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Maesbox\OGInspectorBundle\Entity\Univers'
        ));
    }
}